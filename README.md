# simple-billing-service 
It's a simple REST api service. This service transfer money from accounts.

Stack: Java 11, Spark, JUnit, Mockito, log4j, Gson.

## Some features of the service
1. If any account has never been in the system. His balance is added to the system through the current transaction. (This solution is necessary to fill the service with some test data. This is reflected only in the implementation of the interfaces)
2. At the moment, the transfer of money is carried out only in integers. I wanted to use the long type for the integer and fractional parts of a number, but in time I did not have time to implement calculations on the fractional part.
3. One transaction is one debit and one credit operation on tow accounts.
4. When financial transactions are taken from the pool, acceptance is blocked.
5. Logging financial transactions must occur in a single transaction to a resource.
6. Updating information on the balances in the resource must occur in a single transaction.
## Application architecture
![alt.tag](https://pp.userapi.com/c852124/v852124089/174e9f/M9MDqmF_zmI.jpg)
## How it works?
The application is launched using the standard Java method public static void main (String [] args) in RunApp(port 8080)
### transfer
POST /transfer

Parameters:

 1. TransactionType - String
 2. from - account id from transfer - int
 3. to - account id to transfer - int
 4. amount - money for transfer
#### balance
GET /balance

Parameters:

- accountId - account that balance need know - int
#### status
GET /status

Parameters:

- hash - hash transaction(sent by money transfer) that status need know - int

