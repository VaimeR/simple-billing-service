import dao.cache.AccountsBalanceCache;
import com.google.gson.Gson;
import factory.DaoFactory;
import factory.TransactionProcessorsFactory;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import shared.TransactionStatuses;
import spark.Spark;
import spark.utils.IOUtils;
import transaction.TransactionsStatusesStorage;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class ApiTest
{
    private static AccountsBalanceCache accountsBalanceCache = Mockito.mock(AccountsBalanceCache.class);
    private static TransactionsStatusesStorage transactionsStatusesStorage = Mockito.mock(TransactionsStatusesStorage.class);

    @BeforeClass
    public static void beforeClass()
    {
        DaoFactory.setAccountsBalanceCache(accountsBalanceCache);
        TransactionProcessorsFactory.setTransactionsStatusesStorage(transactionsStatusesStorage);

        RunApp.main(null);
    }

    @AfterClass
    public static void afterClass()
    {
        Spark.stop();
    }

    @Test
    public void transfer_correctTransaction() throws IOException
    {
        TestResponse res = request("POST", "/transfer?type=DEBIT&from=1&to=2&amount=100");
        Map<String, String> answerMap = res.json();
        assertEquals(200, res.status);
        assertEquals(TransactionStatuses.PENDING, TransactionStatuses.valueOf(answerMap.get(answerMap.keySet().toArray()[0])));
    }

    @Test(expected = Exception.class)
    public void transfer_accountFromAndToEquals() throws IOException
    {
        request("POST", "/transfer?type=DEBIT&from=1&to=1&amount=100");
    }

    @Test(expected = Exception.class)
    public void transfer_amountIsZero() throws IOException
    {
        request("POST", "/transfer?type=DEBIT&from=1&to=2&amount=0");
    }

    @Test(expected = Exception.class)
    public void transfer_someAccountIsZero() throws IOException
    {
        request("POST", "/transfer?type=DEBIT&from=1&to=0&amount=100");
    }

    @Test(expected = Exception.class)
    public void transfer_invalidTypeTransaction() throws IOException
    {
        request("POST", "/transfer?type=DEB&from=1&to=0&amount=100");
    }

    @Test
    public void getBalance_accountWithoutTransactions() throws IOException
    {
        TestResponse res = request("GET", "/balance?accountId=1");
        assertEquals(Long.valueOf(0), Long.valueOf(res.body));
    }

    @Test
    public void getBalance_accountWithTransactions() throws IOException
    {
        when(accountsBalanceCache.getBalance(1)).thenReturn((Long.valueOf(100)));

        TestResponse res = request("GET", "/balance?accountId=1");
        assertEquals(Long.valueOf(100), Long.valueOf(res.body));
    }

    @Test(expected = Exception.class)
    public void getBalance_sendInvalidAccountId() throws IOException
    {
        request("GET", "/balance?accountId=df");
    }

    @Test
    public void getStatus_statusSuccess() throws IOException
    {
        when(transactionsStatusesStorage.getStatus(1)).thenReturn(TransactionStatuses.SUCCESS);

        TestResponse res = request("GET", "/status?hash=1");
        assertEquals(TransactionStatuses.SUCCESS, TransactionStatuses.valueOf(res.body.replace("\"", "")));
    }

    @Test
    public void getStatus_statusPending() throws IOException
    {
        when(transactionsStatusesStorage.getStatus(1)).thenReturn(TransactionStatuses.PENDING);

        TestResponse res = request("GET", "/status?hash=1");
        assertEquals(TransactionStatuses.PENDING, TransactionStatuses.valueOf(res.body.replace("\"", "")));
    }

    @Test
    public void getStatus_haveNotTransactionsHash() throws IOException
    {
        TestResponse res = request("GET", "/status?hash=2");
        assertNull(res.json());
    }

    @Test(expected = Exception.class)
    public void getStatus_invalidTransactionHash() throws IOException
    {
        request("GET", "/status?hash=df");
    }

    private TestResponse request(String method, String path) throws IOException
    {
        try
        {
            URL url = new URL("http://localhost:8080" + path);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(method);
            connection.setDoOutput(true);
            connection.connect();
            String body = IOUtils.toString(connection.getInputStream());
            return new TestResponse(connection.getResponseCode(), body);
        } catch (IOException e)
        {
            System.out.println("Sending request failed: " + e.getMessage());
            throw e;
        }
    }

    private static class TestResponse
    {
        final String body;
        final int status;

        TestResponse(int status, String body)
        {
            this.status = status;
            this.body = body;
        }

        HashMap json()
        {
            return new Gson().fromJson(body, HashMap.class);
        }
    }
}
