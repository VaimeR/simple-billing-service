package job;

import org.junit.Test;
import org.mockito.Mockito;
import shared.Operation;
import shared.Transaction;
import transaction.*;
import utils.TestUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.*;

public class LogTransactionJobTest
{
    private UpdateBalancesProcessor updateBalancesProcessor = Mockito.mock(UpdateBalancesProcessor.class);
    private LogOperationsProcessor logOperationsProcessor = Mockito.mock(LogOperationsProcessor.class);
    private MergeTransactionProcessor mergeTransactionProcessor = Mockito.mock(MergeTransactionProcessor.class);
    private TransactionPool transactionPool = Mockito.mock(TransactionPool.class);
    private TransactionsStatusesStorage transactionsStatusesStorage = Mockito.mock(TransactionsStatusesStorage.class);

    private LogTransactionJob logTransactionJob = new LogTransactionJob(updateBalancesProcessor,
                                                                        logOperationsProcessor,
                                                                        mergeTransactionProcessor,
                                                                        transactionPool,
                                                                        transactionsStatusesStorage);

    @Test
    public void run_verifyCorrectMethodSequence()
    {
        List<Transaction> transactions = new ArrayList<>();
        transactions.add(TestUtils.getCreditTransaction());
        when(transactionPool.getPoolAndErase()).thenReturn(transactions);

        Map<Long, Map<OperationsTypes, Operation>> operationMap = TestUtils.getAccountsOperationMap();
        when(mergeTransactionProcessor.merge(transactions)).thenReturn(operationMap);

        doNothing().when(logOperationsProcessor).logOperations(operationMap);
        doNothing().when(updateBalancesProcessor).updateBalances(operationMap);

        logTransactionJob.run();

        verify(transactionPool).getPoolAndErase();
        verify(mergeTransactionProcessor).merge(transactions);
        verify(logOperationsProcessor).logOperations(operationMap);
        verify(updateBalancesProcessor).updateBalances(operationMap);
    }
}
