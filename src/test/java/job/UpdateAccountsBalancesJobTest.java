package job;

import dao.AccountsBalancesDao;
import dao.LogOperationsDao;
import org.junit.Test;
import org.mockito.Mockito;
import shared.Operation;
import utils.TestUtils;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

public class UpdateAccountsBalancesJobTest
{
    private LogOperationsDao logOperationsDao = Mockito.mock(LogOperationsDao.class);
    private AccountsBalancesDao accountsBalancesDao = Mockito.mock(AccountsBalancesDao.class);

    private UpdateAccountsBalancesJob updateAccountsBalancesJob = new UpdateAccountsBalancesJob(logOperationsDao, accountsBalancesDao);

    @Test
    public void run_verifyCorrectMethodSequence()
    {
        List<Operation> operations = new ArrayList<>();
        operations.add(TestUtils.getCreditOperation());
        operations.add(TestUtils.getDebitOperation());

        when(logOperationsDao.getNotMarkOperation()).thenReturn(operations);
        doNothing().when(accountsBalancesDao).updateBalances(operations);

        updateAccountsBalancesJob.run();

        verify(logOperationsDao).getNotMarkOperation();
        verify(accountsBalancesDao).updateBalances(operations);
    }
}
