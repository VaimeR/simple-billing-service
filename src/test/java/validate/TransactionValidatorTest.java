package validate;

import org.junit.Test;
import shared.Transaction;
import transaction.OperationsTypes;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TransactionValidatorTest
{
    @Test
    public void isValid_validTransaction()
    {
        Transaction transaction = new Transaction(OperationsTypes.DEBIT, 1, 2, 300);

        assertTrue(TransactionValidator.isValid(transaction));
    }

    @Test
    public void isValid_senderAndReceiverEquals()
    {
        Transaction transaction = new Transaction(OperationsTypes.DEBIT, 1, 1, 300);

        assertFalse(TransactionValidator.isValid(transaction));
    }

    @Test
    public void isValid_senderIsZero()
    {
        Transaction transaction = new Transaction(OperationsTypes.DEBIT, 0, 1, 300);

        assertFalse(TransactionValidator.isValid(transaction));
    }

    @Test
    public void isValid_receiverIsZero()
    {
        Transaction transaction = new Transaction(OperationsTypes.DEBIT, 1, 0, 300);

        assertFalse(TransactionValidator.isValid(transaction));
    }

    @Test
    public void isValid_amountIsZero()
    {
        Transaction transaction = new Transaction(OperationsTypes.DEBIT, 1, 2, 0);

        assertFalse(TransactionValidator.isValid(transaction));
    }

    @Test
    public void isValid_invalidType()
    {
        Transaction transaction = new Transaction(OperationsTypes.INVALID, 1, 2, 0);

        assertFalse(TransactionValidator.isValid(transaction));
    }
}
