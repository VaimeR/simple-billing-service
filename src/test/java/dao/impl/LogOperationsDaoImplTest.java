package dao.impl;

import org.junit.Test;
import shared.Operation;
import utils.TestUtils;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class LogOperationsDaoImplTest
{
    private LogOperationsDaoImpl logOperationsDao = new LogOperationsDaoImpl();

    @Test
    public void log_checkCorrectLogOperation()
    {
        Operation operation = TestUtils.getDebitOperation();

        logOperationsDao.log(operation);

        List<Operation> result = logOperationsDao.getNotMarkOperation();

        assertEquals(1, result.size());
        assertEquals(operation, result.get(0));
    }

    @Test
    public void getNotMarkOperation_checkGettingNotMarkOperations()
    {
        Operation operation1 = TestUtils.getDebitOperation();
        Operation operation2 = TestUtils.getCreditOperation();

        logOperationsDao.log(operation1);
        logOperationsDao.log(operation2);

        List<Operation> result = logOperationsDao.getNotMarkOperation();

        assertEquals(2, result.size());
        assertEquals(operation1, result.get(0));
        assertEquals(operation2, result.get(1));
    }
}
