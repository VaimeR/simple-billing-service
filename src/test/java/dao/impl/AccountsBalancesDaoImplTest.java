package dao.impl;

import dao.AccountsBalancesDao;
import org.junit.Test;
import shared.Operation;
import utils.TestUtils;

import java.util.ArrayList;
import java.util.List;

public class AccountsBalancesDaoImplTest
{
    private AccountsBalancesDao accountsBalancesDao = new AccountsBalancesDaoImpl();

    @Test
    public void updateBalances_tryUpdateBalances()
    {
        List<Operation> operations = new ArrayList<>();

        operations.add(TestUtils.getDebitOperation());
        operations.add(TestUtils.getCreditOperation());

        accountsBalancesDao.updateBalances(operations);
    }
}
