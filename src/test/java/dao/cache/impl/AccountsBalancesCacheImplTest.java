package dao.cache.impl;

import dao.cache.AccountsBalanceCache;
import org.junit.Test;

import java.util.concurrent.atomic.AtomicLong;

import static org.junit.Assert.assertEquals;

public class AccountsBalancesCacheImplTest
{
    private final static long accountId = 1;
    private final static long amount = 300;

    private AccountsBalanceCache accountsBalanceCache = new AccountsBalanceCacheImpl();

    @Test
    public void updateDebitBalances_correctUpdateBalance()
    {
        accountsBalanceCache.updateDebitBalances(accountId, new AtomicLong(amount));

        assertEquals(amount, accountsBalanceCache.getBalance(accountId));
    }

    @Test
    public void updateCreditBalances_correctUpdateBalance()
    {
        accountsBalanceCache.updateCreditBalances(accountId, new AtomicLong(amount));

        assertEquals(-amount, accountsBalanceCache.getBalance(accountId));
    }

    @Test
    public void getBalance_checkCorrectDifferentBetweenDebitCredit()
    {
        accountsBalanceCache.updateDebitBalances(accountId, new AtomicLong(amount));
        accountsBalanceCache.updateDebitBalances(accountId, new AtomicLong(amount));
        accountsBalanceCache.updateCreditBalances(accountId, new AtomicLong(amount));

        assertEquals(amount, accountsBalanceCache.getBalance(accountId));
    }

    @Test
    public void updateBalances_checkCorrectDifferentBetweenDebitCredit()
    {
        accountsBalanceCache.updateBalances(accountId,
                                            new AtomicLong(amount + amount),
                                            new AtomicLong(amount));

        assertEquals(amount, accountsBalanceCache.getBalance(accountId));
    }
}
