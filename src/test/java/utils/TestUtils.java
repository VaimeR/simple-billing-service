package utils;

import shared.Operation;
import shared.Transaction;
import transaction.OperationsTypes;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class TestUtils
{
    public static long accountId = 1;
    public static AtomicLong credit = new AtomicLong(300);
    public static AtomicLong debit = new AtomicLong(200);

    public static Operation getDebitOperation()
    {
        return new Operation(OperationsTypes.DEBIT, accountId, debit);
    }

    public static Operation getCreditOperation()
    {
        return new Operation(OperationsTypes.CREDIT, accountId, credit);
    }

    public static Transaction getCreditTransaction()
    {
        return new Transaction(OperationsTypes.CREDIT, accountId, 2, 300);
    }

    public static Map<Long, Map<OperationsTypes, Operation>> getAccountsOperationMap()
    {
        Map<Long, Map<OperationsTypes, Operation>> accountOperationMap = new HashMap<>();
        accountOperationMap.put(accountId, new HashMap<>()
        {{
            put(OperationsTypes.DEBIT, getDebitOperation());
            put(OperationsTypes.CREDIT, getCreditOperation());
        }});

        return accountOperationMap;
    }
}
