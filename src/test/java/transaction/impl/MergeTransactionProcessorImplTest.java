package transaction.impl;


import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import org.junit.Test;
import shared.Operation;
import shared.Transaction;
import transaction.MergeTransactionProcessor;
import transaction.OperationsTypes;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class MergeTransactionProcessorImplTest
{
    private final static long accountId1 = 1;
    private final static long accountId2 = 2;
    private final static long amount1 = 500;
    private final static long amount2 = 300;

    private MergeTransactionProcessor mergeTransactionProcessor = new MergeTransactionProcessorImpl();

    @Test
    public void merge_mergeOneDebitTransaction()
    {
        Transaction transaction = new Transaction(OperationsTypes.DEBIT, accountId1, accountId2, amount1);
        List<Transaction> transactions = new ArrayList<>();
        transactions.add(transaction);

        Operation debitOperation = new Operation(OperationsTypes.DEBIT, accountId1, new AtomicLong(amount1));
        Operation creditOperation = new Operation(OperationsTypes.CREDIT, accountId2, new AtomicLong(amount1));

        Map<Long, Map<OperationsTypes, Operation>> result = mergeTransactionProcessor.merge(transactions);

        assertEquals(2, result.size());
        assertEquals(1, result.get(accountId1).size());
        assertEquals(1, result.get(accountId2).size());
        assertEquals(debitOperation, result.get(accountId1).get(OperationsTypes.DEBIT));
        assertEquals(creditOperation, result.get(accountId2).get(OperationsTypes.CREDIT));
    }

    @Test
    public void merge_mergeOneCreditTransaction()
    {
        Transaction transaction = new Transaction(OperationsTypes.CREDIT, accountId1, accountId2, amount1);
        List<Transaction> transactions = new ArrayList<>();
        transactions.add(transaction);

        Operation creditOperation= new Operation(OperationsTypes.CREDIT, accountId1, new AtomicLong(amount1));
        Operation debitOperation = new Operation(OperationsTypes.DEBIT, accountId2, new AtomicLong(amount1));

        Map<Long, Map<OperationsTypes, Operation>> result = mergeTransactionProcessor.merge(transactions);

        assertEquals(2, result.size());
        assertEquals(1, result.get(accountId1).size());
        assertEquals(1, result.get(accountId2).size());
        assertEquals(creditOperation, result.get(accountId1).get(OperationsTypes.CREDIT));
        assertEquals(debitOperation, result.get(accountId2).get(OperationsTypes.DEBIT));
    }

    @Test
    public void merge_mergeWithSumTransaction()
    {
        Transaction transaction1 = new Transaction(OperationsTypes.DEBIT, accountId1, accountId2, amount1);
        Transaction transaction2 = new Transaction(OperationsTypes.CREDIT, accountId2, accountId1, amount2);

        List<Transaction> transactions = new ArrayList<>();
        transactions.add(transaction1);
        transactions.add(transaction2);

        Operation debitOperation = new Operation(OperationsTypes.DEBIT, accountId1, new AtomicLong(amount1 + amount2));
        Operation creditOperation = new Operation(OperationsTypes.CREDIT, accountId2, new AtomicLong(amount1 + amount2));

        Map<Long, Map<OperationsTypes, Operation>> result = mergeTransactionProcessor.merge(transactions);

        assertEquals(2, result.size());
        assertEquals(1, result.get(accountId1).size());
        assertEquals(1, result.get(accountId2).size());
        assertEquals(debitOperation, result.get(accountId1).get(OperationsTypes.DEBIT));
        assertEquals(creditOperation, result.get(accountId2).get(OperationsTypes.CREDIT));
    }

    @Test
    public void merge_mergeTransaction()
    {
        Transaction transaction1 = new Transaction(OperationsTypes.DEBIT, accountId1, accountId2, amount1);
        Transaction transaction2 = new Transaction(OperationsTypes.CREDIT, accountId1, accountId2, amount2);

        List<Transaction> transactions = new ArrayList<>();
        transactions.add(transaction1);
        transactions.add(transaction2);

        Operation debitOperation1= new Operation(OperationsTypes.DEBIT, accountId1, new AtomicLong(amount1));
        Operation creditOperation1= new Operation(OperationsTypes.CREDIT, accountId2, new AtomicLong(amount1));
        Operation debitOperation2 = new Operation(OperationsTypes.DEBIT, accountId2, new AtomicLong(amount2));
        Operation creditOperation2 = new Operation(OperationsTypes.CREDIT, accountId1, new AtomicLong(amount2));

        Map<Long, Map<OperationsTypes, Operation>> result = mergeTransactionProcessor.merge(transactions);

        assertEquals(2, result.size());
        assertEquals(2, result.get(accountId1).size());
        assertEquals(2, result.get(accountId2).size());
        assertEquals(debitOperation1, result.get(accountId1).get(OperationsTypes.DEBIT));
        assertEquals(creditOperation2, result.get(accountId1).get(OperationsTypes.CREDIT));
        assertEquals(debitOperation2, result.get(accountId2).get(OperationsTypes.DEBIT));
        assertEquals(creditOperation1, result.get(accountId2).get(OperationsTypes.CREDIT));
    }
}
