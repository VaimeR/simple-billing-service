package transaction.impl;

import dao.LogOperationsDao;
import org.junit.Test;
import org.mockito.Mockito;
import shared.Operation;
import transaction.LogOperationsProcessor;
import transaction.OperationsTypes;
import utils.TestUtils;

import java.util.Map;

import static org.mockito.Mockito.verify;

public class LogOperationsProcessorImplTest
{
    private LogOperationsDao logOperationsDao = Mockito.mock(LogOperationsDao.class);
    private LogOperationsProcessor logOperationsProcessor = new LogOperationsProcessorImpl(logOperationsDao);

    @Test
    public void logOperations_verifyUsingLogOperationDao()
    {
        Map<Long, Map<OperationsTypes, Operation>> accountOperationMap = TestUtils.getAccountsOperationMap();

        logOperationsProcessor.logOperations(accountOperationMap);

        verify(logOperationsDao).log(TestUtils.getCreditOperation());
        verify(logOperationsDao).log(TestUtils.getDebitOperation());
    }
}
