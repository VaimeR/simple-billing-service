package transaction.impl;

import org.junit.Test;
import shared.Transaction;
import shared.TransactionStatuses;
import transaction.OperationsTypes;
import transaction.TransactionsStatusesStorage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class TransactionsStatusesStorageImplTest
{
    private TransactionsStatusesStorage transactionsStatusesStorage = new TransactionsStatusesStorageImpl();

    @Test
    public void updateStatuses_correctTransactionsList()
    {
        Transaction transaction = new Transaction(OperationsTypes.CREDIT, 1, 2, 100);
        List<Transaction> list = new ArrayList<>();
        list.add(transaction);

        transactionsStatusesStorage.updateStatuses(list, TransactionStatuses.PENDING);

        assertEquals(TransactionStatuses.PENDING, transactionsStatusesStorage.getStatus(transaction.hashCode()));
    }

    @Test
    public void updateStatuses_emptyTransactionList()
    {
        List list = Collections.EMPTY_LIST;

        transactionsStatusesStorage.updateStatuses(list, TransactionStatuses.PENDING);

        assertNull(transactionsStatusesStorage.getStatus(1));
    }

    @Test
    public void updateStatus_checkCorrectUpdateStatusesOfTransaction()
    {
        transactionsStatusesStorage.updateStatus(2, TransactionStatuses.PENDING);

        assertEquals(TransactionStatuses.PENDING, transactionsStatusesStorage.getStatus(2));
    }

    @Test
    public void getStatus_haveNotInformationAboutThisTransaction()
    {
        transactionsStatusesStorage.updateStatus(2, TransactionStatuses.SUCCESS);

        assertNull(transactionsStatusesStorage.getStatus(3));
    }

    @Test
    public void getStatus_checkCorrectGetStatusesOfTransaction()
    {
        transactionsStatusesStorage.updateStatus(2, TransactionStatuses.SUCCESS);

        assertEquals(TransactionStatuses.SUCCESS, transactionsStatusesStorage.getStatus(2));
    }
}
