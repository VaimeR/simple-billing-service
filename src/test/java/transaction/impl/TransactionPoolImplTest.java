package transaction.impl;

import org.junit.Test;
import shared.Transaction;
import transaction.OperationsTypes;
import transaction.TransactionPool;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class TransactionPoolImplTest
{
    private TransactionPool transactionPool = new TransactionPoolImpl();

    @Test
    public void add_verifyAddTransaction()
    {
        Transaction transaction = new Transaction(OperationsTypes.CREDIT, 1, 2, 100);

        transactionPool.add(transaction);

        List<Transaction> transactions = transactionPool.getPoolAndErase();

        assertEquals(1, transactions.size());
        assertEquals(transaction, transactions.get(0));
    }

    @Test
    public void getPoolAndErase_verifyErase()
    {
        Transaction transaction = new Transaction(OperationsTypes.CREDIT, 1, 2, 100);
        transactionPool.add(transaction);

        transactionPool.getPoolAndErase();
        List<Transaction> transactions = transactionPool.getPoolAndErase();

        assertEquals(0, transactions.size());
    }
}
