package transaction.impl;

import dao.cache.AccountsBalanceCache;
import org.junit.Test;
import org.mockito.Mockito;
import shared.Operation;
import transaction.OperationsTypes;
import transaction.UpdateBalancesProcessor;
import utils.TestUtils;

import java.util.Map;

import static org.mockito.Mockito.verify;

public class UpdateBalancesProcessorImplTest
{
    private AccountsBalanceCache accountsBalanceCache = Mockito.mock(AccountsBalanceCache.class);

    private UpdateBalancesProcessor updateBalancesProcessor = new UpdateBalancesProcessorImpl(accountsBalanceCache);

    @Test
    public void updateBalances_verifyUsingUpdateBalanceCache()
    {
        Map<Long, Map<OperationsTypes, Operation>> accountOperationMap = TestUtils.getAccountsOperationMap();

        updateBalancesProcessor.updateBalances(accountOperationMap);

        verify(accountsBalanceCache).updateBalances(TestUtils.accountId,
                                                    TestUtils.getDebitOperation().getAmount(),
                                                    TestUtils.getCreditOperation().getAmount());
    }
}
