import dao.cache.AccountsBalanceCache;
import exception.InvalidRequestParameters;
import exception.InvalidTransactionException;
import shared.Transaction;
import shared.TransactionBuilder;
import shared.TransactionStatuses;
import spark.Request;
import spark.Response;
import transaction.TransactionPool;
import transaction.TransactionsStatusesStorage;
import validate.TransactionValidator;

import java.util.Map;

/**
 * Makes request processing and delegate requests to another objects.
 */
public class RequestDelegate
{
    private TransactionPool transactionPool;
    private AccountsBalanceCache accountsBalanceCache;
    private TransactionsStatusesStorage transactionsStatusesStorage;

    public RequestDelegate(TransactionPool transactionPool,
                           AccountsBalanceCache accountsBalanceCache,
                           TransactionsStatusesStorage transactionsStatusesStorage)
    {
        this.transactionPool = transactionPool;
        this.accountsBalanceCache = accountsBalanceCache;
        this.transactionsStatusesStorage = transactionsStatusesStorage;
    }

    public Object transfer(Request req, Response res) throws Exception
    {
        Transaction transaction = TransactionBuilder.create()
                                                    .setAmount(req.queryParams("amount"))
                                                    .setFrom(req.queryParams("from"))
                                                    .setTo(req.queryParams("to"))
                                                    .setOperationType(req.queryParams("type"))
                                                    .build();

        if (!TransactionValidator.isValid(transaction))
        {
            throw new InvalidTransactionException();
        }

        transactionPool.add(transaction);
        transactionsStatusesStorage.updateStatus(transaction.hashCode(), TransactionStatuses.PENDING);

        res.type("application/json");

        return Map.entry(transaction.hashCode(), TransactionStatuses.PENDING);
    }

    public Object getBalance(Request req, Response res) throws InvalidRequestParameters
    {
        long accountId;

        try
        {
            accountId = Long.valueOf(req.queryParams("accountId"));
        }
        catch (NumberFormatException e)
        {
            throw new InvalidRequestParameters();
        }

        res.type("application/json");

        return accountsBalanceCache.getBalance(accountId);
    }

    public Object getStatus(Request req, Response res) throws InvalidRequestParameters
    {
        int hash;

        try
        {
            hash = Integer.valueOf(req.queryParams("hash"));
        }
        catch (NumberFormatException e)
        {
            throw new InvalidRequestParameters();
        }

        return transactionsStatusesStorage.getStatus(hash);
    }
}
