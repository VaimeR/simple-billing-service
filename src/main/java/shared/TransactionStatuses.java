package shared;

public enum TransactionStatuses
{
    PENDING,
    FAILING,
    SUCCESS;
}
