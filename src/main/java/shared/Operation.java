package shared;

import transaction.OperationsTypes;

import java.util.concurrent.atomic.AtomicLong;

public class Operation
{
    private OperationsTypes operationType;

    private long accountId;

    private AtomicLong amount;

    public Operation(OperationsTypes operationType, long accountId)
    {
        this.operationType = operationType;
        this.accountId = accountId;
        this.amount = new AtomicLong(0);
    }

    public Operation(OperationsTypes operationType, long accountId, AtomicLong amount)
    {
        this.operationType = operationType;
        this.accountId = accountId;
        this.amount = amount;
    }

    public OperationsTypes getOperationType()
    {
        return operationType;
    }

    public void setOperationType(OperationsTypes operationType)
    {
        this.operationType = operationType;
    }

    public long getAccountId()
    {
        return accountId;
    }

    public void setAccountId(long accountId)
    {
        this.accountId = accountId;
    }

    public AtomicLong getAmount()
    {
        return amount;
    }

    public void setAmount(AtomicLong amount)
    {
        this.amount = amount;
    }

    public void addToAmount(long amount)
    {
        this.amount.addAndGet(amount);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Operation)) return false;

        Operation operation = (Operation) o;

        if (accountId != operation.getAccountId()) return false;
        if (operationType != operation.getOperationType()) return false;
        return amount.get() == operation.amount.get();

    }

    @Override
    public int hashCode()
    {
        int result = operationType.hashCode();
        result = 31 * result + (int) (accountId ^ (accountId >>> 32));
        result = 31 * result + amount.hashCode();
        return result;
    }
}
