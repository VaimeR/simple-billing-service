package shared;

import transaction.OperationsTypes;

public class Transaction
{
    private final OperationsTypes operationType;

    private final long from;

    private final long to;

    private final long amount;

    public Transaction(OperationsTypes operationType,
                       long from,
                       long to,
                       long amount)
    {
        this.operationType = operationType;
        this.from = from;
        this.to = to;
        this.amount = amount;
    }

    public OperationsTypes getOperationType()
    {
        return operationType;
    }

    public long getFrom()
    {
        return from;
    }

    public long getTo()
    {
        return to;
    }

    public long getAmount()
    {
        return amount;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Transaction)) return false;

        Transaction that = (Transaction) o;

        if (from != that.from) return false;
        if (to != that.to) return false;
        if (amount != that.amount) return false;
        return operationType == that.operationType;

    }

    @Override
    public int hashCode()
    {
        int result = operationType.hashCode();
        result = 31 * result + (int) (from ^ (from >>> 32));
        result = 31 * result + (int) (to ^ (to >>> 32));
        result = 31 * result + (int) (amount ^ (amount >>> 32));
        return result;
    }
}
