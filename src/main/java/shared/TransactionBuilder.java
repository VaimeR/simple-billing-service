package shared;

import transaction.OperationsTypes;

public class TransactionBuilder
{
    private OperationsTypes operationType;
    private long from;
    private long to;
    private long amount;

    private TransactionBuilder()
    {
    }

    public static TransactionBuilder create()
    {
        return new TransactionBuilder();
    }

    public Transaction build()
    {
        return new Transaction(operationType, from, to, amount);
    }

    public TransactionBuilder setOperationType(String operationType)
    {
        try
        {
            this.operationType = OperationsTypes.valueOf(operationType);
        }
        catch (Exception e)
        {
            this.operationType = OperationsTypes.INVALID;
        }

        return this;
    }

    public TransactionBuilder setFrom(String from)
    {
        this.from = tryCastStringToLong(from);

        return this;
    }

    public TransactionBuilder setTo(String to)
    {
        this.to = tryCastStringToLong(to);

        return this;
    }

    public TransactionBuilder setAmount(String amount)
    {
        this.amount = tryCastStringToLong(amount);

        return this;
    }

    private long tryCastStringToLong(String value)
    {
        try
        {
            return Long.valueOf(value);
        }
        catch (NumberFormatException e)
        {
            return 0;
        }
    }
}
