package shared;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class SystemSettings
{
    private static SystemSettings instance;

    private int httpPort;
    private int updateBalancesInitialDelay;
    private int updateBalancesDelay;
    private int logTransactionInitialDelay;
    private int logTransactionDelay;
    private TimeUnit updateBalancesTimeUnit;
    private TimeUnit logTransactionTimeUnit;

    public static SystemSettings getInstance()
    {
        if (instance != null)
        {
            return instance;
        }

        return initialize();
    }

    public int getHttpPort()
    {
        return httpPort;
    }

    private void setHttpPort(int httpPort)
    {
        this.httpPort = httpPort;
    }

    public int getUpdateBalancesInitialDelay()
    {
        return updateBalancesInitialDelay;
    }

    private void setUpdateBalancesInitialDelay(int updateBalancesInitialDelay)
    {
        this.updateBalancesInitialDelay = updateBalancesInitialDelay;
    }

    public int getUpdateBalancesDelay()
    {
        return updateBalancesDelay;
    }

    private void setUpdateBalancesDelay(int updateBalancesDelay)
    {
        this.updateBalancesDelay = updateBalancesDelay;
    }

    public int getLogTransactionInitialDelay()
    {
        return logTransactionInitialDelay;
    }

    private void setLogTransactionInitialDelay(int logTransactionInitialDelay)
    {
        this.logTransactionInitialDelay = logTransactionInitialDelay;
    }

    public int getLogTransactionDelay()
    {
        return logTransactionDelay;
    }

    private void setLogTransactionDelay(int logTransactionDelay)
    {
        this.logTransactionDelay = logTransactionDelay;
    }

    public TimeUnit getUpdateBalancesTimeUnit()
    {
        return updateBalancesTimeUnit;
    }

    private void setUpdateBalancesTimeUnit(TimeUnit updateBalancesTimeUnit)
    {
        this.updateBalancesTimeUnit = updateBalancesTimeUnit;
    }

    public TimeUnit getLogTransactionTimeUnit()
    {
        return logTransactionTimeUnit;
    }

    private void setLogTransactionTimeUnit(TimeUnit logTransactionTimeUnit)
    {
        this.logTransactionTimeUnit = logTransactionTimeUnit;
    }

    private static SystemSettings initialize()
    {
        try (InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream("settings.properties"))
        {

            Properties prop = new Properties();

            // load a properties file
            prop.load(input);

            SystemSettings systemSettings = new SystemSettings();

            systemSettings.setHttpPort(Integer.valueOf(prop.getProperty("http.port")));
            systemSettings.setLogTransactionDelay(Integer.valueOf(prop.getProperty("logTransaction.delay")));
            systemSettings.setLogTransactionInitialDelay(Integer.valueOf((prop.getProperty("logTransaction.initialDelay"))));
            systemSettings.setLogTransactionTimeUnit(TimeUnit.valueOf(prop.getProperty("updateBalances.timeUnit")));
            systemSettings.setUpdateBalancesDelay(Integer.valueOf(prop.getProperty("updateBalances.delay")));
            systemSettings.setUpdateBalancesInitialDelay(Integer.valueOf(prop.getProperty("updateBalances.initialDelay")));
            systemSettings.setUpdateBalancesTimeUnit(TimeUnit.valueOf(prop.getProperty("logTransaction.timeUnit")));

            instance = systemSettings;

            return instance;

        } catch (IOException ex)
        {
            ex.printStackTrace();
        }

        return null;
    }
}
