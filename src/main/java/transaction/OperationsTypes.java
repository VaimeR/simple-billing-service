package transaction;

public enum OperationsTypes
{
    DEBIT,
    CREDIT,
    INVALID;
}
