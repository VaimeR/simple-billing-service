package transaction;

import shared.Operation;
import shared.Transaction;

import java.util.List;
import java.util.Map;

/**
 * Merge all transactions bu one account to one operation
 */
public interface MergeTransactionProcessor
{
    Map<Long, Map<OperationsTypes, Operation>> merge(List<Transaction> transactions);
}
