package transaction;

import shared.Operation;

import java.util.Map;

/**
 * Log operation in data source
 */
public interface LogOperationsProcessor
{
    void logOperations(Map<Long, Map<OperationsTypes, Operation>> operationsMap);
}
