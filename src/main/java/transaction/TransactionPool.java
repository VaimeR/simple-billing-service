package transaction;

import shared.Transaction;

import java.util.List;

/**
 * Store All received transactions
 */
public interface TransactionPool
{
    void add(Transaction transaction);

    List<Transaction> getPoolAndErase();
}
