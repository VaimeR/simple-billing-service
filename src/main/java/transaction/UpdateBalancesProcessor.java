package transaction;

import shared.Operation;

import java.util.Map;

/**
 * Update balance in Balance cache
 */
public interface UpdateBalancesProcessor
{
    void updateBalances(Map<Long, Map<OperationsTypes, Operation>> operationsMap);
}
