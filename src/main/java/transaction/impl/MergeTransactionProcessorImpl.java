package transaction.impl;

import shared.Operation;
import shared.Transaction;
import transaction.MergeTransactionProcessor;
import transaction.OperationsTypes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MergeTransactionProcessorImpl implements MergeTransactionProcessor
{
    @Override
    public Map<Long, Map<OperationsTypes, Operation>> merge(List<Transaction> transactions)
    {
        Map<Long, Map<OperationsTypes, Operation>> mergedTransactions = new HashMap<>();

        transactions.forEach(currentTransaction ->
        {
            OperationsTypes currentOperationType = currentTransaction.getOperationType();
            OperationsTypes inverseOperationType = getInverseOperation(currentOperationType);

            Operation operationTo = getAndCreateOperationMap(mergedTransactions, currentTransaction.getTo()).get(inverseOperationType);
            Operation operationFrom = getAndCreateOperationMap(mergedTransactions, currentTransaction.getFrom()).get(currentOperationType);

            operationFrom = updateOperationAmount(currentTransaction.getFrom(),
                                                  currentTransaction.getAmount(),
                                                  currentOperationType,
                                                  operationFrom);

            operationTo = updateOperationAmount(currentTransaction.getTo(),
                                                currentTransaction.getAmount(),
                                                inverseOperationType,
                                                operationTo);

            getAndCreateOperationMap(mergedTransactions, currentTransaction.getFrom()).put(currentOperationType, operationFrom);
            getAndCreateOperationMap(mergedTransactions, currentTransaction.getTo()).put(inverseOperationType, operationTo);
        });

        return mergedTransactions;
    }

    private Operation updateOperationAmount(long accountId, long amount, OperationsTypes type, Operation operation)
    {
        if(operation == null)
        {
            operation = new Operation(type, accountId);
        }

        operation.addToAmount(amount);

        return operation;
    }

    private Map<OperationsTypes, Operation> getAndCreateOperationMap(Map<Long, Map<OperationsTypes, Operation>> mergedTransactions,
                                                                     long accountId)
    {
        return mergedTransactions.computeIfAbsent(accountId, k -> new HashMap<>());
    }


    private OperationsTypes getInverseOperation(OperationsTypes type)
    {
        return type.equals(OperationsTypes.DEBIT) ? OperationsTypes.CREDIT : OperationsTypes.DEBIT;
    }
}
