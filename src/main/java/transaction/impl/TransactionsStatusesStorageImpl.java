package transaction.impl;

import shared.Transaction;
import shared.TransactionStatuses;
import transaction.TransactionsStatusesStorage;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class TransactionsStatusesStorageImpl implements TransactionsStatusesStorage
{
    Map<Integer, TransactionStatuses> statuses;

    public TransactionsStatusesStorageImpl()
    {
        this.statuses = new ConcurrentHashMap<>();
    }

    @Override
    public void updateStatuses(List<Transaction> transactions, TransactionStatuses status)
    {
        transactions.forEach(transaction -> {
            statuses.put(transaction.hashCode(), status);
        });
    }

    @Override
    public void updateStatus(int transactionHash, TransactionStatuses status)
    {
        statuses.put(transactionHash, status);
    }

    @Override
    public TransactionStatuses getStatus(int transactionHash)
    {
        return statuses.get(transactionHash);
    }
}
