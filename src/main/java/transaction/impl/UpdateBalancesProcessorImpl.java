package transaction.impl;

import dao.cache.AccountsBalanceCache;
import shared.Operation;
import transaction.OperationsTypes;
import transaction.UpdateBalancesProcessor;

import java.util.Map;

public class UpdateBalancesProcessorImpl implements UpdateBalancesProcessor
{
    private AccountsBalanceCache accountsBalanceCache;

    public UpdateBalancesProcessorImpl(AccountsBalanceCache accountsBalanceCache)
    {
        this.accountsBalanceCache = accountsBalanceCache;
    }

    @Override
    public void updateBalances(Map<Long, Map<OperationsTypes, Operation>> operationsMap)
    {
        operationsMap.forEach((accountId, operationMap) -> {
            Operation debitOperation = operationMap.get(OperationsTypes.DEBIT);
            Operation creditOperation = operationMap.get(OperationsTypes.CREDIT);

            accountsBalanceCache.updateBalances(accountId,
                                                debitOperation.getAmount(),
                                                creditOperation.getAmount());
        });
    }
}
