package transaction.impl;

import dao.LogOperationsDao;
import shared.Operation;
import transaction.LogOperationsProcessor;
import transaction.OperationsTypes;

import java.util.Map;

public class LogOperationsProcessorImpl implements LogOperationsProcessor
{
    private LogOperationsDao logOperationsDAO;

    public LogOperationsProcessorImpl(LogOperationsDao logOperationsDAO)
    {
        this.logOperationsDAO = logOperationsDAO;
    }

    @Override
    public void logOperations(Map<Long, Map<OperationsTypes, Operation>> operationsMap)
    {
        operationsMap.forEach((accountId, operationMap) ->
            operationMap.forEach((operationType, operation) -> logOperationsDAO.log(operation))
        );
    }
}
