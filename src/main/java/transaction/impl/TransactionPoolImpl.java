package transaction.impl;

import shared.Transaction;
import transaction.TransactionPool;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class TransactionPoolImpl implements TransactionPool
{
    private List<Transaction> transactions;
    private ReentrantLock lock;

    public TransactionPoolImpl()
    {
        this.transactions = new ArrayList<>();
        this.lock = new ReentrantLock();
    }

    public void add(Transaction transaction)
    {
        //TODO I dont like see this lock. How delete it?
        lock.lock();

        try
        {
            transactions.add(transaction);
        }
        finally
        {
            lock.unlock();
        }
    }

    public List<Transaction> getPoolAndErase()
    {
        lock.lock();

        try
        {
            List<Transaction> wholePool = transactions;
            transactions = new ArrayList<>();

            return wholePool;
        }
        finally
        {
            lock.unlock();
        }
    }
}
