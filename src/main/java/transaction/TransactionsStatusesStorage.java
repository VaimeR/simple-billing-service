package transaction;

import shared.Transaction;
import shared.TransactionStatuses;

import java.util.List;

public interface TransactionsStatusesStorage
{
    void updateStatuses(List<Transaction> transactions, TransactionStatuses status);

    void updateStatus(int transactionHash, TransactionStatuses status);

    TransactionStatuses getStatus(int transactionHash);
}
