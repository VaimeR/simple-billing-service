package factory;

import transaction.*;
import transaction.impl.*;

public class TransactionProcessorsFactory
{
    private static LogOperationsProcessor logOperationsProcessor;
    private static TransactionPool transactionPool;
    private static UpdateBalancesProcessor updateBalancesProcessor;
    private static MergeTransactionProcessor mergeTransactionProcessor;
    private static TransactionsStatusesStorage transactionsStatusesStorage;

    public static LogOperationsProcessor getLogOperationsDao()
    {
        if(logOperationsProcessor == null)
        {
            logOperationsProcessor = new LogOperationsProcessorImpl(DaoFactory.getLogOperationsDao());
        }

        return logOperationsProcessor;
    }

    public static TransactionPool getTransactionPool()
    {
        if(transactionPool == null)
        {
            transactionPool = new TransactionPoolImpl();
        }

        return transactionPool;
    }

    public static UpdateBalancesProcessor getUpdateBalancesProcessor()
    {
        if(updateBalancesProcessor == null)
        {
            updateBalancesProcessor = new UpdateBalancesProcessorImpl(DaoFactory.getAccountsBalanceCache());
        }

        return updateBalancesProcessor;
    }

    public static MergeTransactionProcessor getMergeTransactionProcessor()
    {
        if(mergeTransactionProcessor == null)
        {
            mergeTransactionProcessor = new MergeTransactionProcessorImpl();
        }

        return mergeTransactionProcessor;
    }

    public static TransactionsStatusesStorage getTransactionsStatusesStorage()
    {
        if(transactionsStatusesStorage == null)
        {
            transactionsStatusesStorage = new TransactionsStatusesStorageImpl();
        }

        return transactionsStatusesStorage;
    }

    // Setters to mocks in tests
    public static void setLogOperationsProcessor(LogOperationsProcessor logOperationsProcessor)
    {
        TransactionProcessorsFactory.logOperationsProcessor = logOperationsProcessor;
    }

    public static void setTransactionPool(TransactionPool transactionPool)
    {
        TransactionProcessorsFactory.transactionPool = transactionPool;
    }

    public static void setUpdateBalancesProcessor(UpdateBalancesProcessor updateBalancesProcessor)
    {
        TransactionProcessorsFactory.updateBalancesProcessor = updateBalancesProcessor;
    }

    public static void setMergeTransactionProcessor(MergeTransactionProcessor mergeTransactionProcessor)
    {
        TransactionProcessorsFactory.mergeTransactionProcessor = mergeTransactionProcessor;
    }

    public static void setTransactionsStatusesStorage(TransactionsStatusesStorage transactionsStatusesStorage)
    {
        TransactionProcessorsFactory.transactionsStatusesStorage = transactionsStatusesStorage;
    }
}
