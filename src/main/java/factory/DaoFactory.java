package factory;

import dao.AccountsBalancesDao;
import dao.LogOperationsDao;
import dao.cache.AccountsBalanceCache;
import dao.cache.impl.AccountsBalanceCacheImpl;
import dao.impl.AccountsBalancesDaoImpl;
import dao.impl.LogOperationsDaoImpl;

public class DaoFactory
{
    private static LogOperationsDao logOperationsDao;
    private static AccountsBalancesDao accountsBalancesDao;
    private static AccountsBalanceCache accountsBalanceCache;

    public static LogOperationsDao getLogOperationsDao()
    {
        if(logOperationsDao == null)
        {
            logOperationsDao = new LogOperationsDaoImpl();
        }

        return logOperationsDao;
    }

    public static AccountsBalancesDao getAccountsBalancesDao()
    {
        if(accountsBalancesDao == null)
        {
            accountsBalancesDao = new AccountsBalancesDaoImpl();
        }

        return accountsBalancesDao;
    }

    public static AccountsBalanceCache getAccountsBalanceCache()
    {
        if(accountsBalanceCache == null)
        {
            accountsBalanceCache = new AccountsBalanceCacheImpl();
        }

        return accountsBalanceCache;
    }

    // Setters to mocks in tests
    public static void setLogOperationsDao(LogOperationsDao logOperationsDao)
    {
        DaoFactory.logOperationsDao = logOperationsDao;
    }

    public static void setAccountsBalancesDao(AccountsBalancesDao accountsBalancesDao)
    {
        DaoFactory.accountsBalancesDao = accountsBalancesDao;
    }

    public static void setAccountsBalanceCache(AccountsBalanceCache accountsBalanceCache)
    {
        DaoFactory.accountsBalanceCache = accountsBalanceCache;
    }
}
