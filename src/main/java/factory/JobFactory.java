package factory;

import job.LogTransactionJob;
import job.UpdateAccountsBalancesJob;

public class JobFactory
{
    private static LogTransactionJob logTransactionJob;
    private static UpdateAccountsBalancesJob updateAccountsBalancesJob;

    public static LogTransactionJob getLogTransactionJob()
    {
        if(logTransactionJob == null)
        {
            logTransactionJob = new LogTransactionJob(TransactionProcessorsFactory.getUpdateBalancesProcessor(),
                                                      TransactionProcessorsFactory.getLogOperationsDao(),
                                                      TransactionProcessorsFactory.getMergeTransactionProcessor(),
                                                      TransactionProcessorsFactory.getTransactionPool(),
                                                      TransactionProcessorsFactory.getTransactionsStatusesStorage());
        }

        return logTransactionJob;
    }

    public static UpdateAccountsBalancesJob getUpdateAccountsBalancesJob()
    {
        if(updateAccountsBalancesJob == null)
        {
            updateAccountsBalancesJob = new UpdateAccountsBalancesJob(DaoFactory.getLogOperationsDao(),
                                                                      DaoFactory.getAccountsBalancesDao());
        }

        return updateAccountsBalancesJob;
    }
}
