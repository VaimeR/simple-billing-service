import factory.DaoFactory;
import factory.TransactionProcessorsFactory;
import spark.Route;

/**
 * Http end points handlers declarations
 */
public class HttpController
{
    private static HttpController instance;

    private static RequestDelegate requestDelegate;

    private HttpController()
    {
        requestDelegate = new RequestDelegate(TransactionProcessorsFactory.getTransactionPool(),
                                              DaoFactory.getAccountsBalanceCache(),
                                              TransactionProcessorsFactory.getTransactionsStatusesStorage());
    }

    public static HttpController getInstance()
    {
        if (instance == null)
        {
            instance = new HttpController();
        }

        return instance;
    }

    public Route transfer = (req, res) -> requestDelegate.transfer(req, res);

    public Route getBalance = (req, res) -> requestDelegate.getBalance(req, res);

    public Route getStatus = (req, res) -> requestDelegate.getStatus(req, res);
}
