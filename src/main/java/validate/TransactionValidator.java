package validate;

import shared.Transaction;
import transaction.OperationsTypes;

public class TransactionValidator
{
    public static boolean isValid(Transaction transaction)
    {
        return !transaction.getOperationType().equals(OperationsTypes.INVALID) &&
               transaction.getTo() > 0 &&
               transaction.getFrom() > 0 &&
               transaction.getFrom() != transaction.getTo() &&
               transaction.getAmount() > 0;
    }
}
