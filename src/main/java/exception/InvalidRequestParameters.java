package exception;

public class InvalidRequestParameters extends Exception
{
    public InvalidRequestParameters()
    {
        super("You send invalid request parameters.");
    }

    public InvalidRequestParameters(String message)
    {
        super(message);
    }
}
