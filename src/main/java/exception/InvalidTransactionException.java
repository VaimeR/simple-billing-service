package exception;

public class InvalidTransactionException extends Exception
{
    public InvalidTransactionException()
    {
        super("The financial transaction parameters are incorrect.");
    }

    public InvalidTransactionException(String message)
    {
        super(message);
    }
}
