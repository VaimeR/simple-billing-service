package job;

import org.apache.log4j.Logger;
import shared.Operation;
import shared.Transaction;
import shared.TransactionStatuses;
import transaction.*;

import java.util.List;
import java.util.Map;

public class LogTransactionJob implements Runnable
{
    private static final Logger logger = Logger.getLogger(LogTransactionJob.class);

    private UpdateBalancesProcessor updateBalancesProcessor;
    private LogOperationsProcessor logOperationsProcessor;
    private MergeTransactionProcessor mergeTransactionProcessor;
    private TransactionPool transactionPool;
    private TransactionsStatusesStorage transactionsStatusesStorage;

    public LogTransactionJob(UpdateBalancesProcessor updateBalancesProcessor,
                             LogOperationsProcessor logOperationsProcessor,
                             MergeTransactionProcessor mergeTransactionProcessor,
                             TransactionPool transactionPool,
                             TransactionsStatusesStorage transactionsStatusesStorage)
    {
        this.updateBalancesProcessor = updateBalancesProcessor;
        this.logOperationsProcessor = logOperationsProcessor;
        this.mergeTransactionProcessor = mergeTransactionProcessor;
        this.transactionPool = transactionPool;
        this.transactionsStatusesStorage = transactionsStatusesStorage;
    }

    @Override
    public void run()
    {
        logger.info("Start job log transaction");

        List<Transaction> transactions = transactionPool.getPoolAndErase();

        Map<Long, Map<OperationsTypes, Operation>> mergedTransactions = mergeTransactionProcessor.merge(transactions);

        logOperationsProcessor.logOperations(mergedTransactions);

        updateBalancesProcessor.updateBalances(mergedTransactions);

        transactionsStatusesStorage.updateStatuses(transactions, TransactionStatuses.SUCCESS);

        logger.info("End job log transaction");
    }
}
