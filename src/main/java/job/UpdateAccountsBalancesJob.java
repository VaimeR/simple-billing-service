package job;


import dao.AccountsBalancesDao;
import dao.LogOperationsDao;
import org.apache.log4j.Logger;
import shared.Operation;

import java.util.List;

public class UpdateAccountsBalancesJob implements Runnable
{
    private static final Logger logger = Logger.getLogger(UpdateAccountsBalancesJob.class);

    private LogOperationsDao logOperationsDao;
    private AccountsBalancesDao accountsBalancesDao;

    public UpdateAccountsBalancesJob(LogOperationsDao logOperationsDao,
                                     AccountsBalancesDao accountsBalancesDao)
    {
        this.logOperationsDao = logOperationsDao;
        this.accountsBalancesDao = accountsBalancesDao;
    }

    @Override
    public void run()
    {
        logger.info("Start job update balances");

        // Mark is empty collection in this implementation.
        // If will db, implementation does mark this operations how processed in one transaction.
        List<Operation> logOperations = logOperationsDao.getNotMarkOperation();

        accountsBalancesDao.updateBalances(logOperations);

        logger.info("End job update balances");
    }
}
