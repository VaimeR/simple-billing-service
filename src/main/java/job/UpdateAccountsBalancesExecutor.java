package job;

import shared.SystemSettings;

import java.util.concurrent.Executors;

public class UpdateAccountsBalancesExecutor
{
    public UpdateAccountsBalancesExecutor(UpdateAccountsBalancesJob job)
    {
        Executors.newSingleThreadScheduledExecutor()
                 .scheduleWithFixedDelay(job,
                                         SystemSettings.getInstance().getUpdateBalancesInitialDelay(),
                                         SystemSettings.getInstance().getUpdateBalancesDelay(),
                                         SystemSettings.getInstance().getUpdateBalancesTimeUnit());
    }
}
