package job;

import shared.SystemSettings;

import java.util.concurrent.Executors;

public class LogTransactionExecutor
{
    public LogTransactionExecutor(LogTransactionJob job)
    {
        Executors.newSingleThreadScheduledExecutor()
                 .scheduleWithFixedDelay(job,
                                         SystemSettings.getInstance().getLogTransactionInitialDelay(),
                                         SystemSettings.getInstance().getLogTransactionDelay(),
                                         SystemSettings.getInstance().getLogTransactionTimeUnit());
    }
}
