import factory.JobFactory;
import job.LogTransactionExecutor;
import job.UpdateAccountsBalancesExecutor;
import org.apache.log4j.Logger;
import shared.SystemSettings;
import utils.JsonUtil;

/**
 * Start class and declaration http endpoint
 */
import static spark.Spark.*;

public class RunApp
{
    private static final Logger logger = Logger.getLogger(RunApp.class);

    public static void main(String[] args)
    {
        logger.info("Start app");

        port(SystemSettings.getInstance().getHttpPort());
        start();
    }

    public static void start()
    {
        logger.info("Initializing routes and jobs");
        executeJobs();
        establishRoutes();
    }

    private static void establishRoutes()
    {
        post("/transfer", "application/json", HttpController.getInstance().transfer, JsonUtil::toJson);
        get("/balance", "application/json", HttpController.getInstance().getBalance, JsonUtil::toJson);
        get("/status", "application/json", HttpController.getInstance().getStatus, JsonUtil::toJson);
    }

    private static void executeJobs()
    {
        LogTransactionExecutor logTransactionExecutor = new LogTransactionExecutor(JobFactory.getLogTransactionJob());
        UpdateAccountsBalancesExecutor updateBalancesExecutor = new UpdateAccountsBalancesExecutor(JobFactory.getUpdateAccountsBalancesJob());
    }
}
