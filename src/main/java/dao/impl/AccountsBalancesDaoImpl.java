package dao.impl;

import dao.AccountsBalancesDao;
import shared.Operation;
import transaction.OperationsTypes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class AccountsBalancesDaoImpl implements AccountsBalancesDao
{
    private Map<Long, Map<OperationsTypes, AtomicLong>> accountsBalances ;

    public AccountsBalancesDaoImpl()
    {
        this.accountsBalances = new ConcurrentHashMap();
    }

    @Override
    public void updateBalances(List<Operation> operations)
    {
        operations.forEach(operation -> {
            Map<OperationsTypes, AtomicLong> balances = getAndCreateBalanceMap(operation.getAccountId());

            updateBalanceByType(operation.getAmount(), operation.getOperationType(), balances);
        });
    }

    private void updateBalanceByType(AtomicLong amount,
                                     OperationsTypes type,
                                     Map<OperationsTypes, AtomicLong> balances)
    {
        if (balances.containsKey(type))
        {
            balances.get(type).addAndGet(amount.get());
        }
        else
        {
            balances.put(type, amount);
        }
    }

    // It is need to fill cache data
    private Map<OperationsTypes, AtomicLong> getAndCreateBalanceMap(long accountId)
    {
        return accountsBalances.computeIfAbsent(accountId, k -> new HashMap<>());
    }
}
