package dao.impl;

import dao.LogOperationsDao;
import shared.Operation;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class LogOperationsDaoImpl implements LogOperationsDao
{
    private List<Operation> logOperations;
    private ReentrantLock reentrantLock;

    public LogOperationsDaoImpl()
    {
        logOperations = new ArrayList<>();
        reentrantLock = new ReentrantLock();
    }

    @Override
    public void log(Operation operation)
    {
        reentrantLock.lock();

        try
        {
            logOperations.add(operation);
        }
        finally
        {
            reentrantLock.unlock();
        }
    }

    @Override
    public List<Operation> getNotMarkOperation()
    {
        reentrantLock.lock();

        try
        {
            // Mark is empty collection in this implementation.
            // If will db, implementation does mark this operations how processed in one transaction.
            List<Operation> operations = logOperations;
            logOperations = new ArrayList<>();

            return operations;
        }
        finally
        {
            reentrantLock.unlock();
        }
    }
}
