package dao;

import shared.Operation;

import java.util.List;

public interface LogOperationsDao
{
    void log(Operation operation);

    List<Operation> getNotMarkOperation();
}
