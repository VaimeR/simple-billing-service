package dao.cache;

import java.util.concurrent.atomic.AtomicLong;

public interface AccountsBalanceCache
{
    void updateBalances(long accountId, AtomicLong debitAmount, AtomicLong creditAmount);

    void updateDebitBalances(long accountId, AtomicLong debitAmount);

    void updateCreditBalances(long accountId, AtomicLong creditAmount);

    long getBalance(long accountId);
}
