package dao.cache.impl;

import dao.cache.AccountsBalanceCache;
import transaction.OperationsTypes;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class AccountsBalanceCacheImpl implements AccountsBalanceCache
{
    private Map<Long, Map<OperationsTypes, AtomicLong>> accountsBalances;

    public AccountsBalanceCacheImpl()
    {
        this.accountsBalances = new ConcurrentHashMap();
    }

    @Override
    public void updateBalances(long accountId, AtomicLong debitAmount, AtomicLong creditAmount)
    {
        Map<OperationsTypes, AtomicLong> balances = getAndCreateBalanceMap(accountId);

        updateBalanceByType(debitAmount, OperationsTypes.DEBIT, balances);
        updateBalanceByType(creditAmount, OperationsTypes.CREDIT, balances);
    }

    @Override
    public void updateDebitBalances(long accountId, AtomicLong debitAmount)
    {
        Map<OperationsTypes, AtomicLong> balances = getAndCreateBalanceMap(accountId);

        updateBalanceByType(debitAmount, OperationsTypes.DEBIT, balances);
    }

    @Override
    public void updateCreditBalances(long accountId, AtomicLong creditAmount)
    {
        Map<OperationsTypes, AtomicLong> balances = getAndCreateBalanceMap(accountId);

        updateBalanceByType(creditAmount, OperationsTypes.CREDIT, balances);
    }

    @Override
    public long getBalance(long accountId)
    {
        Map<OperationsTypes, AtomicLong> balances = getAndCreateBalanceMap(accountId);

        AtomicLong debitBalance = balances.getOrDefault(OperationsTypes.DEBIT, new AtomicLong());
        AtomicLong creditBalance = balances.getOrDefault(OperationsTypes.CREDIT, new AtomicLong());

        return debitBalance.get() - creditBalance.get();
    }

    private void updateBalanceByType(AtomicLong amount,
                                     OperationsTypes type,
                                     Map<OperationsTypes, AtomicLong> balances)
    {
        if (balances.containsKey(type))
        {
            balances.get(type).addAndGet(amount.get());
        }
        else
        {
            balances.put(type, amount);
        }
    }

    // It is need to fill cache data
    private Map<OperationsTypes, AtomicLong> getAndCreateBalanceMap(long accountId)
    {
        return accountsBalances.computeIfAbsent(accountId, k -> new HashMap<>());
    }
}
