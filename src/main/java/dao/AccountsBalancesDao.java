package dao;

import shared.Operation;

import java.util.List;

public interface AccountsBalancesDao
{
    void updateBalances(List<Operation> operations);
}
